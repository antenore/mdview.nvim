"=============================================================================
" FILE: mdview.vim
" AUTHOR:  Antenore Gatta <antenore at gmail.com>
" License: MIT license
"=============================================================================

let g:term_buf = 0
let g:term_win = 0
function! TermToggle(length)
    silent execute 'update | edit'
    set scrollbind
    exec 'syncbind'
    let s:current_buffer_file_path = expand('%')
    let s:current_curs_pos = getpos('.')
    let s:mdview_command = 'mdview -i ' . s:current_buffer_file_path
    if win_gotoid(g:term_win)
        " We completely remove the buffer
        bw!
        "hide
    else
        """ horizontal split
        "botright new
        "exec "resize " . a:length

        """ vertical split
        botright vnew
        exec 'vertical resize ' . a:length
        try
            exec 'buffer ' . g:term_buf
        catch
            call termopen(s:mdview_command, {'detach': 0})
            let g:term_buf = bufnr('')
            call setpos('.', s:current_curs_pos)
            setlocal scrollbind
            exec 'syncbind'
            setlocal nonumber
            setlocal norelativenumber
            setlocal signcolumn=no
            setlocal nocursorline
            setlocal nocursorcolumn
        endtry
        let g:term_win = win_getid()
    endif
endfunction


" Toggle terminal on/off (neovim)
nnoremap <F29> :call TermToggle(15)<CR>
inoremap <F29> <Esc>:call TermToggle(12)<CR>
tnoremap <F29> <C-\><C-n>:call TermToggle(12)<CR>

tnoremap :q! <C-\><C-n>:q!<CR>
