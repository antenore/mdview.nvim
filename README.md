# Mdview.nvim

A Vim and Neovim Markdown previewer

When editing a Markdown file, pressing CTRL+F5 (F29), a new buffer will open with
the preview of your Markdown file.

## Installation

To install `Mdview.nvim`, use your plugin manager of choice, e.g:

```
Plug 'https://gitlab.com/antenore/mdview.nvim'
```
